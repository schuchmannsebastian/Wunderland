# Wunderland P6
## Components
### ESP Scripts
All the scripts for the ESP8266. We are using this Microcontroller 5 times. They all use the MQTT Protocol for communication. 
### NodeJS Scripts
The MQTT Broker runs on NodeJS and is needed for a MQTT Network to function.  
### Unity Projects and Files
Here the main project is located.
## How to start
You need to start the MQTT Broker via NPM Start in the Nodejs Scripts/MQTTBroker folder (Node.js needed). Start Unity. Start all microcontrollers.  



*MADE FOR WUNDERLAND AT H_DA GERMANY*