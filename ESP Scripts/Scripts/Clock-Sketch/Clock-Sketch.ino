#include <ESP8266WiFi.h>
#include <PubSubClient.h>

/*
 * WRITTEN BY SEBASTIAN SCHUCHMANN
 * CLOCK SKETCH (BASIC ROTARY ENCODER AND MQTT)
 *
 * MADE FOR WUNDERLAND AT H_DA GERMANY
 * 05.07.2018 ------------------------
 */


#define inputA D3
#define inputB D2

WiFiClient espClient;
PubSubClient client(espClient);

// WIFI Values
const char* ssid = "Wunderland";
const char* password = "deinemutterisstpausenbrot";
const char* mqtt_server = "192.168.0.100";
const int mqtt_port = 1883;

//MQTT Messages
char msg[50];
int messagesIntervalInMilli = 10000;
long lastMsg = 0;

//Rotary Encoder Variables
int referenceValue = 0;
int val = 0;
int counter = 0;
int aState = 0;
int aLastState = 0;

void setup() {
    //Serial
    Serial.begin(115200);
    //Connecting to WIFI
    setup_wifi();

    //Setup MQTT
    client.setServer(mqtt_server, mqtt_port);
    client.setCallback(callback);

    //Setup Rotary Encoder
    pinMode (inputA,INPUT);
    pinMode (inputB,INPUT);
    aLastState = digitalRead(inputA);

    //Setup LED
    pinMode(BUILTIN_LED, OUTPUT); // Initialize the BUILTIN_LED pin as an output
}

//MAIN LOOP
void loop() {
    if (!client.connected()) {
        reconnect();
    }
    client.loop();

    //Only sends are message if there was a change
    bool shouldSendMSG = updateRotary();

    if(shouldSendMSG) {
        sendRotation();
    } else { //Send a Message somtimes even when it didnt change
        long now = millis(); //Some Seconds have passed
        if (now - lastMsg > messagesIntervalInMilli) {
            lastMsg = now;
            sendRotation();
        }
    }
}

//Sends the the current Rotation to MQTT
void sendRotation() {
    //Convert Value to the right format
    String pubString = String(val);
    pubString.toCharArray(msg, pubString.length()+1);
    //Publish to MQTT
    client.publish("UhrRotation", msg);
}

bool updateRotary() {
    bool changed = false;

    aState = digitalRead(inputA); // Reads the "current" state

    // If the previous and the current state are different, that means a Pulse has occured
    if (aState != aLastState) {
        // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
        if (digitalRead(inputB) != aState) {
            val++;
            changed = true;
        } else {
          val--;
            changed = true;
        }
    } else {
        changed = false;
    }

    aLastState = aState; // Updates the previous state of the outputA with the current state
    return changed;
}

//MQTT Reconnect
void reconnect() {
    // Loop until we're reconnected
    while (!client.connected()) {
        // Attempt to connect
        if (client.connect("CLOCK")) {
            client.publish("General", "Clock has successfully connected!");
        } else {
            Serial.print(client.state());
            // Wait 2 seconds before retrying
            delay(2000);
        }
    }
}

//Recieved a Message
void callback(char* topic, byte* payload, unsigned int length) {
    Serial.print("Message arrived: ");
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
    }

    //When recieving a the referenceValue it has to be converted
    int recievedMSG = (int)payload;
    payload[length] = '\0';
    String s = String((char*)payload);
    referenceValue = s.toInt();

}

void setup_wifi() {
    delay(10);
//Connecting to a WIFI
    Serial.println();
    Serial.print("Connecting..");
    WiFi.begin(ssid, password);

//Is connected?
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("WiFi connected");
}

