/* 
 * WRITTEN BY SEBASTIAN SCHUCHMANN
 * BASIC MQTT PUBSUB SCRIPT FOR ESP8266
 * 
 * MADE FOR WUNDERLAND AT H_DA GERMANY
 * 30.06.2018 ------------------------
 */
 
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

WiFiClient espClient;
PubSubClient client(espClient);

// WIFI Values
const char* ssid = "Wunderland";
const char* password = "deinemutterisstpausenbrot";
const char* mqtt_server = "192.168.0.100";
const int mqtt_port = 1883;

//MQTT Messages
char msg[50];
int messagesIntervalInMilli = 20;
long lastMsg = 0;


//MAIN LOOP
void loop() {
  if (!client.connected()) {
    reconnect();}
  client.loop();

  long now = millis();
  //Send a message every messagesIntervalInMilli
  if (now - lastMsg > messagesIntervalInMilli) {
    lastMsg = now;

    //String to CharArray to align the format
    String pubString = "MESSAGE TO PUBLISH";
    pubString.toCharArray(msg, pubString.length()+1); 
    client.publish("SET TOPIC", msg);
  }
}

//MQTT Reconnect
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    // Attempt to connect
    if (client.connect("PUT IN NAME HERE!")) {
      client.publish("PUT IN TOPIC", "PUT IN MESSAGE");
      client.subscribe("PUT IN TOPIC HERE");
    } else {
      Serial.print(client.state());
      // Wait 2 seconds before retrying
      delay(2000);
    }
  }
}

//Recieved a Message
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived: ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
}

void setup() {
  //Serial
  Serial.begin(115200);
  //Connecting to WIFI
  setup_wifi();
  
  //Setup MQTT
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
}

void setup_wifi() { 
  delay(10);
 //Connecting to a WIFI
  Serial.println();
  Serial.print("Connecting..");
  WiFi.begin(ssid, password);

//Is connected?
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
}

