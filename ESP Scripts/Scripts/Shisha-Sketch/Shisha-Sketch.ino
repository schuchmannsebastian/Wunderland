#include <SPI.h>
#include <MFRC522.h>

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
/*
 * WRITTEN BY SEBASTIAN SCHUCHMANN
 * Shisha uses an RFID reader to detect if the Kraut was
 * laid on top. Talks via MQTT
 *
 * MADE FOR WUNDERLAND AT H_DA GERMANY
 * 05.07.2018 ------------------------
 */

#define SS_PIN 2 //D4
#define RST_PIN 0 //D3

const char* ssid = "Wunderland";
const char* password = "deinemutterisstpausenbrot";
const char* mqtt_server = "192.168.0.100";
const int mqtt_port = 1883;


WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
int count = 0;
String pubString = String("False");
bool rfidActive = false;
int messagesIntervalInMilli = 10000;

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
int statuss = 0;
int out = 0;
void setup()
{
    Serial.begin(9600);   // Initiate a serial communication
    SPI.begin();      // Initiate  SPI bus
    mfrc522.PCD_Init();   // Initiate MFRC522

    //Serial.begin(115200);
    setup_wifi();
    client.setServer(mqtt_server, 1883);
    client.setCallback(callback);
}

void setup_wifi() {

    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
    }
    Serial.println();

    // Switch on the LED if an 1 was received as first character
    if ((char)payload[0] == '1') {
        digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
        // but actually the LED is on; this is because
        // it is acive low on the ESP-01)
    } else {
        digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
    }

}

void reconnect() {
    // Loop until we're reconnected
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        if (client.connect("Shisha")) {
            Serial.println("connected");
            client.publish("General", "Shisha has connected!");

            // Once connected, publish an announcement...
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(10);
        }
    }
}

void loop()
{
    if (!client.connected()) {
        reconnect();
    }
    client.loop();
    delay(10);
    //To reliable check if it was really activated
    if(count > 2) {
        pubString = String("False");
        pubString.toCharArray(msg, pubString.length()+1);
        if(rfidActive) {
            rfidActive = false;
            client.publish("RFIDShisha", msg);
        }
    }
    else {
        pubString = String("True");
        pubString.toCharArray(msg, pubString.length()+1);
        if(!rfidActive) {
            rfidActive = true;
            client.publish("RFIDShisha", msg);
        }
    }

    long now = millis();
    if (now - lastMsg > messagesIntervalInMilli) {
        client.publish("RFIDShisha", msg);
        lastMsg = now;
    }

    // Look for new cards
    if ( ! mfrc522.PICC_IsNewCardPresent())
    {
        count++;
        return;
    }
    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
    {
        Serial.print("SECONDSTRING");
        return;
    }
    //Show UID on serial monitor
    Serial.println();
    Serial.print(" UID tag :");
    String content= "";
    byte letter;
    for (byte i = 0; i < mfrc522.uid.size; i++)
    {
        Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
        Serial.print(mfrc522.uid.uidByte[i], HEX);
        content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
        content.concat(String(mfrc522.uid.uidByte[i], HEX));
    }
    content.toUpperCase();
    Serial.println("hallo");
    if (content.substring(1) == "6A A0 2C 83") //change UID of the card that you want to give access
    {
        count = 0;
        statuss = 1;
    }

    else   {
        count++;
    }

}
