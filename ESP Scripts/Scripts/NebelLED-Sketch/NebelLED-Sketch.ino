#include <ESP8266WiFi.h>
#include <PubSubClient.h>

/*
 * WRITTEN BY SEBASTIAN SCHUCHMANN
 * TURNS SMOKE MACHINE AND LEDS ON/OFF VIA RELAY
 *
 * MADE FOR WUNDERLAND AT H_DA GERMANY
 * 05.07.2018 ------------------------
 */

#define ledRelaisPin D5
#define nebelRelaisPin D2

WiFiClient espClient;
PubSubClient client(espClient);

// WIFI Values
const char* ssid = "Wunderland";
const char* password = "deinemutterisstpausenbrot";
const char* mqtt_server = "192.168.0.100";
const int mqtt_port = 1883;

//MQTT Messages
char msg[50];
int messagesIntervalInMilli = 20;
long lastMsg = 0;

bool ledOn = false;
bool nebelOn = false;


//MAIN LOOP
void loop() {
    if (!client.connected()) {
        reconnect();
    }
    client.loop();
}

//MQTT Reconnect
void reconnect() {
    // Loop until we're reconnected
    while (!client.connected()) {
        // Attempt to connect
        if (client.connect("SMOKE/LED")) {
            client.publish("General", "Smoke/LED connected");
            client.subscribe("LEDGO");
            client.subscribe("NebelGO");
        } else {
            Serial.print(client.state());
            // Wait 2 seconds before retrying
            delay(2000);
        }
    }
}

//Recieved a Message
void callback(char* topic, byte* payload, unsigned int length) {
    Serial.println("Message arrived: ");
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
    }
    Serial.println("MESSAGE RECIEVED");

    String msgTopic = String((char*)topic);
    // -- LED TOPIC --
    if(msgTopic == "LEDGO") {
        payload[length] = '\0';
        String msg = String((char*)payload);
        Serial.println("MESSAGE: " + msg);
        if(msg == "False") {
            Serial.println("TURNING LED OFF");
            digitalWrite(ledRelaisPin, HIGH); //An dieser Stelle würde das Relais einsschalten   }
        } else {
            Serial.println("TURNING LED ON");
            digitalWrite(ledRelaisPin, LOW); //An dieser Stelle würde das Relais einsschalten
        }

    }
    // -- NEBEL TOPIC --
    if(msgTopic == "NebelGO") {
        payload[length] = '\0';
        String msg = String((char*)payload);
        if(msg == "False") {
            Serial.println(msgTopic + " - " + msg + " NEBEL AUS");
            digitalWrite(nebelRelaisPin, HIGH); //An dieser Stelle würde das Relais einsschalten
        } else {
            Serial.println(msgTopic + " - " + msg + " NEBEL AUS");
            digitalWrite(nebelRelaisPin, LOW); //An dieser Stelle würde das Relais einsschalten
        }


    }
}

void setup() {
    //Serial
    Serial.begin(115200);
    //Connecting to WIFI
    setup_wifi();

    //Setup MQTT
    client.setServer(mqtt_server, mqtt_port);
    client.setCallback(callback);

    client.subscribe("LEDGO");
    client.subscribe("NebelGO");

    pinMode(ledRelaisPin, OUTPUT);
    pinMode(nebelRelaisPin, OUTPUT);
    digitalWrite(ledRelaisPin, HIGH); //An dieser Stelle würde das Relais einsschalten

}

void setup_wifi() {
    delay(10);
//Connecting to a WIFI
    Serial.println();
    Serial.print("Connecting..");
    WiFi.begin(ssid, password);

//Is connected?
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("WiFi connected a");
}


