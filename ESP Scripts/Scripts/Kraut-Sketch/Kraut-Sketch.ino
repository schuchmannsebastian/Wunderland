#include <ESP8266WiFi.h>
#include <PubSubClient.h>
/*
 * WRITTEN BY SEBASTIAN SCHUCHMANN
 * KRAUT SCRIPT WITH LEDS AND MQTT
 *
 * MADE FOR WUNDERLAND AT H_DA GERMANY
 * 05.07.2018 ------------------------
 */

WiFiClient espClient;
PubSubClient client(espClient);

int bluePin= D1;
int redPin = D6;
int greenPin = D8;

// WIFI Values
const char* ssid = "Wunderland";
const char* password = "deinemutterisstpausenbrot";
const char* mqtt_server = "192.168.0.100";
const int mqtt_port = 1883;

//State Values
bool krautTaken = false;
bool listenToClock = false; //Save the Values from UhrRotation Topic
bool onShisha = false;
int UhrRotationSaved = -100000000;
int lastUhrRotation = 0;
int amountToMove = 1;
bool plantOpen = false;

//MQTT Messages
char msg[50];
int messagesIntervalInMilli = 20;
long lastMsg = 0;
int counter=0;

void setup() {
    //Serial
    Serial.begin(115200);
    //Connecting to WIFI
    setup_wifi();

    //LEDS
    pinMode(redPin, OUTPUT);
    pinMode(greenPin, OUTPUT);
    pinMode(bluePin, OUTPUT);
    //Start Color

    //Setup MQTT
    client.setServer(mqtt_server, mqtt_port);
    client.setCallback(callback);

    //Subscriptions
    client.subscribe("UhrRotation");
    client.subscribe("listenToClock");
    client.subscribe("RFIDShisha");
    client.subscribe("PflanzeGrownUp");
    client.subscribe("BlumeZuMachen");

    setColor(0, 255, 0);
}


void setColor(int redValue, int greenValue, int blueValue) {
    analogWrite(redPin, redValue);
    analogWrite(greenPin, greenValue);
    analogWrite(bluePin, blueValue);
}

//MAIN LOOP
void loop() {
//MQTT Check
    if (!client.connected()) {
        reconnect();
    }
    client.loop();

    //Kraut was now taken
    if(plantOpen){
    if(listenToClock) { //Uhr was moved
        if(lastUhrRotation != UhrRotationSaved && UhrRotationSaved != -100000000) {
            setColor(255,13,255);
            //KrautTrocken
            client.publish("KrautTrocken", "True");
        } //Normal State
    } else {
        setColor(0, 255, 0);
    }
    } else {
      //No light
      setColor(0, 0, 0);
    }
}

//MQTT Reconnect
void reconnect() {
    // Loop until we're reconnected
    while (!client.connected()) {
        // Attempt to connect
        if (client.connect("KRAUT")) {
             client.subscribe("listenToClock");
    client.subscribe("RFIDShisha");
    client.subscribe("PflanzeGrownUp");
    client.subscribe("BlumeZuMachen");
    client.subscribe("UhrRotation");
            client.publish("General", "Clock has successfully connected!");
            client.publish("KrautTrocken", "False");
        } else {
            Serial.print(client.state());
            // Wait 2 seconds before retrying
            delay(2000);
        }
    }
}

//Recieved a Message
void callback(char* topic, byte* payload, unsigned int length) {
    payload[length] = '\0';
    String topicMSG = String((char*)topic);
    Serial.println("Topic: " + topicMSG + " - " +  String((char*)payload));
    //Kraut Taken


    if(topicMSG == "KrautTaken") {
        payload[length] = '\0';
        String msg = String((char*)payload);
        if(msg == "False") {
            krautTaken = false;
        } else {
            krautTaken = true;

        }
    }

    //Listen To Clock
    if(topicMSG == "listenToClock") {
        payload[length] = '\0';
        String msg = String((char*)payload);
        Serial.println("MSG OF LISTEN TO CLOCK: " + msg);
        if(msg == "True") {
            Serial.println("MESSAGET TRUE");
            listenToClock = true;
            UhrRotationSaved = lastUhrRotation;
            Serial.println("SAVED: " + String(UhrRotationSaved));
            client.publish("KrautTrocken", "False");
        } else { //Reset
            listenToClock = false;
            UhrRotationSaved = -100000000;
        }

    }

     if(topicMSG == "PflanzeGrownUp") {
      Serial.println("Plants Grown Up");
     plantOpen = true;
     }
      if(topicMSG == "BlumeZuMachen") {
      plantOpen = false;
      }

    //UhrRotation
    if(topicMSG == "UhrRotation") {
        int UhrRotation = (int)payload;
        payload[length] = '\0';
        String s = String((char*)payload);
        UhrRotation= s.toInt();
        lastUhrRotation = UhrRotation;
    }

    //RFID Shisha
    if(topicMSG == "RFIDShisha") {
        payload[length] = '\0';
        String msg = String((char*)payload);
        if(msg == "True") {


            onShisha = true;
        } else { //Reset
            onShisha = false;
            client.publish("KrautTrocken", "False");
        }
    }
}


void setup_wifi() {
    delay(10);
//Connecting to a WIFI
    Serial.println();
    Serial.print("Connecting..");
    WiFi.begin(ssid, password);

//Is connected?
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("WiFi connected");
}


