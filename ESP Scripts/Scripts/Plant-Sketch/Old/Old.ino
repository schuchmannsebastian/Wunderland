#include <Servo.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

/*
 * WRITTEN BY ROBIN BITTLINGER AND SEBASTIAN SCHUCHMANN
 * TURN A THE SERVO MOTOR OF THE PLANT VIA MQTT
 *
 * MADE FOR WUNDERLAND AT H_DA GERMANY
 * 05.07.2018 ------------------------
 */

Servo servo;
WiFiClient espClient;
PubSubClient client(espClient);

/// WIFI Values
const char* ssid = "Wunderland";
const char* password = "deinemutterisstpausenbrot";
const char* mqtt_server = "192.168.0.100";
const int mqtt_port = 1883;

//MQTT Messages
char msg[50];
int messagesIntervalInMilli = 20;
long lastMsg = 0;
int counter=0;
int value=0;

int UhrRotationSaved;
bool listenToClock = false;
int lastUhrRotation = 0;
int UhrRotation ;
bool BlumeOffen = false;
bool BlumeNichtZu = false;

void setup() {
    servo.attach(D8);  //D4
    pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
    Serial.begin(115200);
    setup_wifi();
    client.setServer(mqtt_server, 1883);
    client.subscribe("UhrRotation");
    client.setCallback(callback);
}

void setup_wifi() {

    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    //subscriptions
    client.subscribe("UhrRotation");
    client.subscribe("listenToClock");
    client.subscribe("PflanzeGrownUp");
    client.subscribe("BlumeZuMachen");

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {

    String topicMSG = String((char*)topic);
    Serial.println(topicMSG);

    // Switch on the LED if an 1 was received as first character
    if ((char)payload[0] == '1') {
        digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
        // but actually the LED is on; this is because
        // it is acive low on the ESP-01)
    } else {
        digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
    }

    if(topicMSG == "UhrRotation") {
        int UhrRotation = (int)payload;
        payload[length] = '\0';
        String i = String((char*)payload);
        UhrRotation = i.toInt();
        lastUhrRotation = UhrRotation;
        listenToClock = true;
        Serial.print(UhrRotation);
        servo.write(UhrRotation);
        if(BlumeNichtZu) {
            servo.write(UhrRotation);
        }
    }

    if(topicMSG == "BlumeZuMachen") {
        BlumeNichtZu = true;
        BlumeOffen = false;
    }

    if(topicMSG == "PflanzeGrownUp" && !BlumeOffen) {
        servo.write(200);
        BlumeOffen = true;
        BlumeNichtZu = false;
    }
}

void reconnect() {
    // Loop until we're reconnected
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        // Attempt to connect
        if (client.connect("PLANT")) {
            Serial.println("connected");
            // Once connected, publish an announcement...
            client.publish("outTopic", "hello world");
            // ... and resubscribe
            client.subscribe("UhrRotation");
            client.subscribe("PflanzeGrownUp");
            client.subscribe("BlumeZuMachen");
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}
void loop() {

    if (!client.connected()) {
        reconnect();
    }
    client.loop();

    long now = millis();
    if (now - lastMsg > 2000) {
        lastMsg = now;
        ++value;
        snprintf (msg, 75, "hello world #%ld", value);
        Serial.print("Publish message: ");
        Serial.println(msg);
        //client.publish("outTopic", msg);
    }
}

