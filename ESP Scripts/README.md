# ESP Scripts
- Clock Sketch (Rotary + MQTT)
- Kraut Sketch (MQTT + LED)
- MQTT-Sketch (It's just a template for an MQTT Client)
- NebelLED Sketch (Relais + MQTT)
- Plant Sketch (RFID + Servo + MQTT)
- Shisha Sketch (RFID + MQTT)



# MQTT Topics
- UhrRotation (The Clock publish its Rotation state for Unity and the Plant, Kraut)
- ReferenceRotation (Unity publishes to reference UhrRotation to Clock Sketch)
- KrautTrocken
- RFIDShisha
- KrautTaken
- PlantOpen
- NebelGO
- LEDGO
- listenToClock
- General
- Reset
- PflanzeGrownUp
- BlumeZuMachen