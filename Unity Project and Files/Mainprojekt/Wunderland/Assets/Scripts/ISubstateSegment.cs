﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISubstateSegment
{
    // Property declaration:
    GameFlowController gameFlowController
    {
        get;
        set;
    }

}

