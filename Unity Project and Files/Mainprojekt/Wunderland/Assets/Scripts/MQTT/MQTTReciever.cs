﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MQTTReciever : MonoBehaviour {

	public MQTTCommunicator mqttCommunicator;
	public string[] Topics;
	private bool hasSubscriptions = false;

	private List<MQTTMessage> lastMessagesRecieved;

	// Use this for initialization
	void Start () {
	}

	public void addTopic(string addedTopic){
		string[] argumentArrayTopic = { addedTopic };
		mqttCommunicator.subscribe (argumentArrayTopic, this);
	}

	//Called from Communicator
	public void sendMessages(List<MQTTMessage> messages){
       // Debug.Log("There are " + messages.Count + " coming in!");
        List<MQTTMessage> newList = new List<MQTTMessage>();

        if (lastMessagesRecieved != null)
        {
            foreach (MQTTMessage msg in messages)
            {
                //Debug.Log("Comparing them to " + lastMessagesRecieved.Count + " existing Messages!");
                //Debug.Log("Comparing " + msg + " ;");
                bool topicIsThere = false;
                foreach (MQTTMessage lmsg in lastMessagesRecieved)
                {
                   
                    if (lmsg.Topic == msg.Topic)
                    {
                        topicIsThere = true;
                        if (lmsg.Message != msg.Message)
                        { //Topic exists but message is different
                            newList.Add(msg);
                            //Debug.Log("Topic exists but message is different - " + " lastmsgtopic: " + lmsg.Topic + " - " + msg.Topic);
                        }
                        else{
                            newList.Add(lmsg);
                        }

                    }
                    else
                    {
                       // Debug.Log("ADD EXISITING TOPIC TO NEW (" + lmsg.Topic + ") -  Message recieved was: " + msg.Topic);
                        newList.Add(lmsg);

                    }

                }
                if (!topicIsThere)
                {//A message with a new topic --> just add
                    newList.Add(msg);
                   // Debug.Log("A message with a new topic - " +  " - " + msg.Topic);
                } else{
                    
                }
            }
        } else {
           // Debug.Log("First time messages coming in !");
            newList = messages;
        }
       // foreach(MQTTMessage newMsg in newList)
      //      Debug.Log("Total Messages - " + " Topic: " + newMsg.Topic + "msg " + newMsg.Message);
       // Debug.Log("-----");
        lastMessagesRecieved = newList;
	}

	public List<MQTTMessage> getLastMessagesRecieved(){
		return lastMessagesRecieved;
	}


	public string getLastMessageOfTopic(string topic){
		if (lastMessagesRecieved != null) {
			foreach (MQTTMessage msg in lastMessagesRecieved) {
				if (msg.Topic == topic)
					return msg.Message;
			}
		}

		return "";
	}

    public int getLastMessageOfTopicInt(string topic){
        Debug.Log("Trying to parse:" + this.getLastMessageOfTopic(topic));
        Debug.Log("Parsed to: " + (int.Parse(this.getLastMessageOfTopic(topic))).ToString());
        return int.Parse(this.getLastMessageOfTopic(topic));
    }

	// Update is called once per frame
	void Update () {
		//Try to subscripe 
		if (!hasSubscriptions) {
//			Debug.Log ("Trying to subscribe");
				hasSubscriptions = mqttCommunicator.subscribe (Topics, this);
		}
	}
}
