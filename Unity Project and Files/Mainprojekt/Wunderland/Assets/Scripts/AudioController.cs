﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*CONTROLS EVERYTHING AUDIO*/
public class AudioController : MonoBehaviour
{

    public AudioSource Music;
    public AudioSource GrinsekatzeAudio;
    public AudioSource RaupenAudio;
    public AudioSource EnviromentSounds;

    public List<AudioClip> AudioClips;
    // Use this for initialization

    public void PlayNewSound(ref AudioSource audioSource, string AudioclipName, float startTime = default(float))
    {
        foreach (AudioClip clip in AudioClips)
        {
            if (clip.name == AudioclipName)
            {
                audioSource.clip = clip;
                audioSource.time = startTime;
                audioSource.PlayScheduled(startTime);
                //audioSource.PlayOneShot(clip);
               
            }
        }

    }
}
