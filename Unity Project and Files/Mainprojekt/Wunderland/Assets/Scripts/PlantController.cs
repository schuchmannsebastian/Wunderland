﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/* THIS CONTROLS ALL PLANT/MUSHROOM ANIMATIONS
 * IT USES THE CLOCK ROTATION */
public class PlantController : MonoBehaviour {

    //TO DO: MAKE A LIST
    public GameObject plant;
    public List<GameObject> plantList;
    private Animator anim;
    private List<Animator> animList;

    /*The Microcontroller never resets its Rotation Value so we
     * have to save a reference Value for it to make sense*/
    private int referenceValue = 0;
    //Time for Animations
    private float time = 0.0f;
    public float timeDivident = 100.0f;

    //This is point where it will just finish the Animation
    public float animateToEndThreshold = 0.9f;
    private bool isActive = false;
    private bool animateToEnd = false;

	void Start () {
        animList = new List<Animator>();
        foreach(GameObject plants in plantList){
            animList.Add(plants.GetComponent<Animator>());
            plants.GetComponent<Animator>().speed = 0.0f;
        }
	}
	
	//Called at a fixed Rate
	void FixedUpdate () {
        //This basically sets the animations to Time
        if(isActive){
            foreach(Animator animPlant in animList){
                animPlant.Play(0, -1, time);  
                if (animateToEnd)
                    animPlant.speed = 1.0f;
            }
        }
	}

    //Main function called from Gameflow Controller that updates the Plants
    public bool setTime(int current){
        Debug.Log("Ref: " + this.referenceValue + " - " + current + " / " + timeDivident);
        Debug.Log((float)(current - referenceValue) / timeDivident);

        //If it goes below Zero -> set ReferenceValue to current
        if (current < referenceValue)
            referenceValue = current;
        
        time = 0.16f + (float)(current - referenceValue) / timeDivident;
        if(time >= animateToEndThreshold){
            isActive = false;
            animateToEnd = true;
            return true;
        }

        return false;
    }

    public void setReferenceValue(int val){
        this.referenceValue = val;
        Debug.Log("REFERENCE VALUE SET: " + referenceValue);
    }

    public void startPlaying(){
        time = 0.0f;

        isActive = true;
    }

    public void Reset()
    {
        setReferenceValue(0);
        setTime(0);
        animateToEnd = false;
        isActive = false;
        time = 0.0f;
        foreach (Animator animPlant in animList)
        {
            animPlant.Play(0, -1, 0.0f);
  
        }

        foreach (GameObject plants in plantList)
        {
            animList.Add(plants.GetComponent<Animator>());
            plants.GetComponent<Animator>().speed = 0.0f;
        }
    }

    public bool currentBiggerThanReference(int current){
        Debug.Log("Checking if " + current + " is bigger than " + referenceValue);
        if (current > referenceValue)
        {
            return true;
        }
        return false;
    }
}
