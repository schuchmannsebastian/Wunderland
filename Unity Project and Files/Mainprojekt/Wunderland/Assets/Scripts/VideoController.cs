﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Video;

/*CONTROLS EVERYTHING VIDEO*/
public class VideoController : MonoBehaviour
{

    public VideoPlayer grinseKatzeVideo;
    public VideoPlayer raupeVideo;
    VideoClip clipToPlay;
    VideoPlayer playerToPlay;
    bool readyForNextClip = true;
    bool clipHasChanged = false;

    public List<VideoClip> videoClips;

    public void PlayVideo(ref VideoPlayer videoPlayer, string name)
    {
     
        videoPlayer.isLooping = false;
        playerToPlay = videoPlayer;
        try
        {
            foreach (VideoClip clip in videoClips)
            {
                if (clip.name == name)
                {
                    Debug.Log("Found a Clip to play!");
                    clipToPlay = clip;
                    clipHasChanged = true;
                    videoPlayer.clip = clipToPlay;
                    videoPlayer.Prepare();

                }
            }

        }
        catch (Exception e) { Debug.Log(e); }
    }

    public void FixedUpdate()
    {
        // Debug.Log("has clip changed? " + clipHasChanged);
        if (clipHasChanged)
        {

            if (clipToPlay != null)
            {
                Debug.Log("Is prepared? " + playerToPlay.isPrepared);
              //  if (playerToPlay.isPrepared)
             //   {
                    Debug.Log("Clip is ready, now playing!");
                    playerToPlay.Play();
                    clipHasChanged = false;

              //  }
            }

        }


    }

    public void PlayVideoLooping(ref VideoPlayer videoPlayer, string name)
    {

        PlayVideo(ref videoPlayer, name);
        videoPlayer.isLooping = true;

    }

    public void SetPlayersActive(bool state)
    {
        grinseKatzeVideo.gameObject.SetActive(state);
        raupeVideo.gameObject.SetActive(state);
    }

    public void Reset()
    {
        //  grinseKatzeVideo.Stop();
        //  raupeVideo.Stop();
        SetPlayersActive(false);
    }



    public bool IsVideoDone(ref VideoPlayer videoPlayer)
    {
        //It can take some Frames before the frameCount is right
        if ((int)videoPlayer.frameCount > 0)
        {
            //Check if last Frame is reached
            if (videoPlayer.frame >= (int)videoPlayer.frameCount) // > is ambigous with long and ulong --> so cast to int
            {
                videoPlayer.Stop();
                return true;
            }
        }

        return false;
    }
}