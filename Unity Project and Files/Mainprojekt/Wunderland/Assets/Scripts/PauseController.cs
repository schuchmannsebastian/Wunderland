﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This Script is used so making Pauses and checking them is easier
//Pause Controller has a collection of all Pause Objects 
//Outside Objects will only talk to the Controller
//The Pause Object has a length (eg. 10 seconds) and progress (eg. 5 seconds)
public class PauseController : MonoBehaviour {
    
    private List<Pause> pauseCollection;
    private int amountOfPauses; //Will be used as the Identificator for Pauses

	void Awake ()
    {
        //Initialisation
        pauseCollection = new List<Pause>();
        amountOfPauses = 0;
	}
	   
    //FixedUpdate called independend of FrameRate
	void FixedUpdate ()
    {
        //Tick each pause
        foreach(Pause pause in pauseCollection)
        {
            if (!pause.isDone)
            pause.Tick(Time.deltaTime);
        }
		
	}

    //This creates a Pause Objects and returns its ID for handling
    public void createNewPause(float lengthOfPause, int ID) { 
        //Create new Pause and add to Collection
        Pause newPause = new Pause(lengthOfPause, ID);
        pauseCollection.Add(newPause);
        amountOfPauses++;
    }

    public bool isDone(int ID)
    {
        //Look for pause with right ID and return the status
        return getPauseByID(ID).isDone;
    }

    //Find pause with matching ID and return it
    public Pause getPauseByID(int ID)
    {
        foreach (Pause pause in pauseCollection)
        {
            if (pause.ID == ID)
                return pause;
        }

        return null;
    }

    public void Reset()
    {
        pauseCollection.Clear();
    }
}

//Simple Object that keeps track of remaining Time
public class Pause
{
    private float lengthOfPause;
    public float timeProgress { get; set; }
    public bool isDone { get; set; }
    public int ID { get; set; }

    //constructor
    public Pause(float lengthOfPause, int ID)
    {
        this.lengthOfPause = lengthOfPause;
        this.ID = ID;
        isDone = false;
        timeProgress = 0.0f;
    }

    //called each Frame
    public void Tick(float deltaTime)
    {
        //Time is Up
        if(timeProgress >= lengthOfPause)
        {
            isDone = true;
        } else
        {
            //Time since last tick gets added
            timeProgress += deltaTime;
        }
    }


}
