﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterLove.StateMachine;

public abstract class Segment : MonoBehaviour, ISubstateSegment
{
    public GameFlowController gameFlowController{get; set;}

    public enum States
    {
        state1,
        state2,
        state3,
		state4
        /*WaitForSignal,
        Pause,
        MusicFade,
        SecondPause,
        IntroVideo*/
    }

    private StateMachine<States> subStateMachine;

    //Debug Information
    public bool debugDisplayInInspector;
    public bool[] stateDisplay;

    void Awake()
    {
        //Start SubStateMachine
        subStateMachine = StateMachine<States>.Initialize(this);
        subStateMachine.ChangeState((States) 0, StateTransition.Overwrite);

        stateDisplay = new bool[States.GetNames(typeof(States)).Length];
    }

     void Update()
    {
        //Bool Array displays SubStates
        if (debugDisplayInInspector) {
            for (int i = 0; i < stateDisplay.Length; i++)
            {
                if (i == (int)subStateMachine.State)
                    stateDisplay[i] = true;
                else
                    stateDisplay[i] = false;
            }
       }     
    }

    private void OnDisable()
    {
        Debug.Log(this.gameObject.name + " got deactivated!");
    }

    private void OnEnable()
    {
        Debug.Log(this.gameObject.name + " got activated! with state:" + subStateMachine.State);
        subStateMachine.ChangeState((States)0, StateTransition.Overwrite);
    }


}
