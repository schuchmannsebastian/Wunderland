﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro : Segment
{
    public new enum States
    {
        WaitForSignal,
        Pause,
        MusicFade,
        SecondPause,
        IntroVideo
    }
}
