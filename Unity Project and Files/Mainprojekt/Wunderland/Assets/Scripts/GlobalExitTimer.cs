﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//IF THE GLOBAL TIMER RUNS OUT, THE PLAYER LOSES
public class GlobalExitTimer : MonoBehaviour
{
    public GameFlowController gameFlowController;
    public Text timerDisplay;

    [Range(0, 20)]
    public int ExitTimeInMinutes = 5;
    private bool TimerEnabled;
    private float ExitTimeInSeconds;
    private float remainingExitTimeInSeconds;


    public void StartTimer()
    {
        ExitTimeInSeconds = (float)ExitTimeInMinutes * 60.0f;
        remainingExitTimeInSeconds = ExitTimeInSeconds;
        TimerEnabled = true;
        Debug.Log("GLOBAL EXIT TIMER STARTED WITH " + ExitTimeInMinutes + " MINUTES!");
    }

    void FixedUpdate()
    {
        if (TimerEnabled)
        {
            remainingExitTimeInSeconds -= Time.deltaTime;
            timerDisplay.text = ((int)remainingExitTimeInSeconds).ToString();
            if (remainingExitTimeInSeconds <= 0.0f)
            {
                Debug.Log("TIME UP");
                //TIMER OVER --> RESET AND LOSE
                gameFlowController.Lose();
                TimerEnabled = false;
                remainingExitTimeInSeconds = ExitTimeInSeconds;
            }
        }
    }

    public void ResetTimer()
    {
        TimerEnabled = false;
        remainingExitTimeInSeconds = ExitTimeInSeconds;
    }
}
