﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MQTTReciever : MonoBehaviour {

	public MQTTCommunicator mqttCommunicator;
	public string[] Topics;
	private bool hasSubscriptions = false;

	private List<string> lastMessagesRecieved;
	//TODO: Create Dicitionary to save the topic and the message
		//private Dictionary<string topic, 


	// Use this for initialization
	void Start () {
	}

	public void sendMessages(List<string> messages){
		Debug.Log ("I got " + messages.Count + " messages");

		lastMessagesRecieved = messages;
	}

	public List<string> getLastMessagesRecieved(){
		return lastMessagesRecieved;
	}

	/*public string getLastMessageOfTopic(string topic){
		foreach (string Message in lastMessagesRecieved) {
			List<string> buffer = new List<string> ();
			//if(topic == 
		}
	}*/
	
	// Update is called once per frame
	void Update () {
		//Try to subscripe 
		if (!hasSubscriptions) {
			Debug.Log ("Trying to subscribe");
			if (mqttCommunicator.isConnected ()) {
				
				hasSubscriptions = mqttCommunicator.subscribe (Topics, this);
			}
		}
	}
}
