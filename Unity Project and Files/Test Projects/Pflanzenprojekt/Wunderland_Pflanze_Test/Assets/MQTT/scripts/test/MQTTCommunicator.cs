﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine.UI;
using System.Threading;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;

using System;

public class MQTTCommunicator : MonoBehaviour {
	//This is the broker
	private MqttClient client;

	//Connection Details
	public string ipAdress = "0.0.0.0";
	public int Port = 8080;
	public bool Localhost = false;

	private string clientId;
	//Cache/Buffer for Messages that come in
	private Dictionary<MQTTReciever, string[]> RecieverTopicCollection;
	private List<MqttMsgPublishEventArgs> packageBuffer;


	// Called before start
	void Awake () {
		if(Localhost)
			ipAdress = "127.0.0.1";
	}

	void Start(){
		
		//Create Client instance
		client = new MqttClient(IPAddress.Parse(ipAdress),Port , false , null ); 

		//register to message received (Callback)
		client.MqttMsgPublishReceived += client_MqttMsgPublishReceived; 
		clientId = Guid.NewGuid().ToString(); 
		//Connect to Broker
		Debug.Log("Connecting ..");
		Thread thread = new Thread(connect);
		thread.Start();

		//Intialize Buffer
		RecieverTopicCollection = new Dictionary<MQTTReciever, string[]> ();
		packageBuffer = new List<MqttMsgPublishEventArgs> ();
	}

	//When a new Message arrives
	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 
		//Add Message to packageBuffer
		packageBuffer.Add (e);
	} 

	void connect(){
		client.Connect (clientId);
	}

	public bool subscribe(string[] Topics, MQTTReciever reciever){
		//Subscribe to given Topics
		try {
		if (client.IsConnected) {
			
				client.Subscribe (Topics, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE }); 
				RecieverTopicCollection.Add (reciever, Topics);
				return true;
			} else {
			return false;
		}}catch(Exception err) {
		return false;
	}
	}

	public void Update(){
		try{
		if(client.IsConnected)
		{
		List<MqttMsgPublishEventArgs> updatePackageBuffer = new List<MqttMsgPublishEventArgs> (packageBuffer);

		//Check each Package
		foreach (MqttMsgPublishEventArgs package in updatePackageBuffer) {
			//Check each Recipient
			foreach (MQTTReciever reciever in RecieverTopicCollection.Keys) {
				//Check each Topic
				List<string> messagesForReciever = new List<string> ();
				foreach (string topic in RecieverTopicCollection[reciever]) {
					//If Topic matches with message --> send
					if (topic == package.Topic) {
						messagesForReciever.Add (System.Text.Encoding.UTF8.GetString (package.Message));
					}
				}
				reciever.sendMessages (messagesForReciever);
			}
		}

		foreach (MqttMsgPublishEventArgs package in updatePackageBuffer)
			packageBuffer.Remove (package);
			}}catch(Exception e){
		}
	
	}

	public bool isConnected(){
		try{
		return client.IsConnected;
		} catch(Exception e){
			return false;
		}
	}
}
