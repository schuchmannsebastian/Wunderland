using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoviePlayer : MonoBehaviour {
    
    public MovieTexture katzeIntro;
    public MovieTexture katzeUhr;
    public MovieTexture katzeRandom1; 
	
	void Start () {
		GetComponent<RawImage>().texture = katzeIntro as MovieTexture;
        
        katzeIntro.Play();
	}
	
	void Update () {
		
	}
}
