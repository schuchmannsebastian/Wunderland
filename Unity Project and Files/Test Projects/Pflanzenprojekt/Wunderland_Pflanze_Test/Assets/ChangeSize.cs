using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ChangeSize : MonoBehaviour {

 public float gain = 1f;

	void Start () {
	}
		
	void Update () {
		this.transform.localScale = Vector3.one * gain;
	}
}

