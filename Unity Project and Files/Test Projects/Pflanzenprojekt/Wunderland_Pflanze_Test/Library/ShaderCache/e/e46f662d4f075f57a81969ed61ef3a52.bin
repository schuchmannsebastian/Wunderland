��                       	"     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _Time;
    float3 _WorldSpaceCameraPos;
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 _CapColor;
    float4 _BaseColor1;
    float4 _BaseColor2;
    float4 _BottomColor;
    float _CapTexScale;
    float _BaseTexScale;
    float _Underglow;
    float _Capglow;
    bool4 unity_MetaFragmentControl;
    float unity_OneOverOutputBoost;
    float unity_MaxOutputValue;
    float unity_UseLinearSpace;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_CapTex [[ sampler (0) ]],
    sampler sampler_BaseTex [[ sampler (1) ]],
    texture2d<float, access::sample > _BaseTex [[ texture (0) ]] ,
    texture2d<float, access::sample > _CapTex [[ texture (1) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    bool u_xlatb0;
    float4 u_xlat1;
    float4 u_xlat2;
    half u_xlat16_2;
    float3 u_xlat3;
    float4 u_xlat4;
    half4 u_xlat10_4;
    bool u_xlatb4;
    float3 u_xlat5;
    float3 u_xlat9;
    float u_xlat10;
    bool u_xlatb10;
    float u_xlat15;
    bool u_xlatb16;
    float u_xlat18;
    float u_xlat19;
    bool u_xlatb19;
    float u_xlat20;
    float u_xlat21;
    u_xlat0.xyz = (-FGlobals._WorldSpaceCameraPos.xyzx.xyz) + FGlobals.hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat0.x = sqrt(u_xlat0.x);
    u_xlatb0 = 50.0<u_xlat0.x;
    if(u_xlatb0){
        u_xlat0.xyz = FGlobals._CapColor.xyz + (-FGlobals._BaseColor1.xyz);
        u_xlat0.xyz = fma(u_xlat0.xyz, float3(0.25, 0.25, 0.25), FGlobals._BaseColor1.xyz);
        u_xlat0.xyz = u_xlat0.xyz * float3(0.5, 0.5, 0.5);
        u_xlat1.x = float(0.0);
        u_xlat1.y = float(0.0);
        u_xlat1.z = float(0.0);
    } else {
        u_xlat2.xyz = input.TEXCOORD1.yyy * FGlobals.hlslcc_mtx4x4unity_WorldToObject[1].xyz;
        u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[0].xyz, input.TEXCOORD1.xxx, u_xlat2.xyz);
        u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[2].xyz, input.TEXCOORD1.zzz, u_xlat2.xyz);
        u_xlat2.yzw = u_xlat2.xyz + FGlobals.hlslcc_mtx4x4unity_WorldToObject[3].xyz;
        u_xlat18 = input.TEXCOORD0.y * FGlobals.hlslcc_mtx4x4unity_WorldToObject[1].y;
        u_xlat18 = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[0].y, input.TEXCOORD0.x, u_xlat18);
        u_xlat18 = fma(FGlobals.hlslcc_mtx4x4unity_WorldToObject[2].y, input.TEXCOORD0.z, u_xlat18);
        u_xlat19 = (-u_xlat18) + -0.400000006;
        u_xlat19 = u_xlat19 * 1.66666663;
        u_xlat19 = clamp(u_xlat19, 0.0f, 1.0f);
        u_xlat3.x = fma(u_xlat19, -2.0, 3.0);
        u_xlat19 = u_xlat19 * u_xlat19;
        u_xlat19 = u_xlat19 * u_xlat3.x;
        u_xlat3.x = u_xlat18;
        u_xlat3.x = clamp(u_xlat3.x, 0.0f, 1.0f);
        u_xlat9.x = fma(u_xlat3.x, -2.0, 3.0);
        u_xlat3.x = u_xlat3.x * u_xlat3.x;
        u_xlat3.x = u_xlat3.x * u_xlat9.x;
        u_xlat18 = -abs(u_xlat18) + 1.0;
        u_xlat18 = u_xlat18 + u_xlat18;
        u_xlat18 = clamp(u_xlat18, 0.0f, 1.0f);
        u_xlat9.x = fma(u_xlat18, -2.0, 3.0);
        u_xlat18 = u_xlat18 * u_xlat18;
        u_xlat18 = u_xlat18 * u_xlat9.x;
        u_xlat9.x = dot(u_xlat2.yw, u_xlat2.yw);
        u_xlat9.x = sqrt(u_xlat9.x);
        u_xlat9.x = u_xlat9.x * 0.5;
        u_xlat9.x = min(u_xlat9.x, 1.0);
        u_xlat15 = u_xlat9.x + -0.100000001;
        u_xlat15 = u_xlat15 * 4.99999952;
        u_xlat15 = clamp(u_xlat15, 0.0f, 1.0f);
        u_xlat21 = fma(u_xlat15, -2.0, 3.0);
        u_xlat15 = u_xlat15 * u_xlat15;
        u_xlat15 = u_xlat15 * u_xlat21;
        u_xlat21 = min(abs(u_xlat2.w), abs(u_xlat2.y));
        u_xlat4.x = max(abs(u_xlat2.w), abs(u_xlat2.y));
        u_xlat4.x = float(1.0) / u_xlat4.x;
        u_xlat21 = u_xlat21 * u_xlat4.x;
        u_xlat4.x = u_xlat21 * u_xlat21;
        u_xlat10 = fma(u_xlat4.x, 0.0208350997, -0.0851330012);
        u_xlat10 = fma(u_xlat4.x, u_xlat10, 0.180141002);
        u_xlat10 = fma(u_xlat4.x, u_xlat10, -0.330299497);
        u_xlat4.x = fma(u_xlat4.x, u_xlat10, 0.999866009);
        u_xlat10 = u_xlat21 * u_xlat4.x;
        u_xlatb16 = abs(u_xlat2.w)<abs(u_xlat2.y);
        u_xlat10 = fma(u_xlat10, -2.0, 1.57079637);
        u_xlat10 = u_xlatb16 ? u_xlat10 : float(0.0);
        u_xlat21 = fma(u_xlat21, u_xlat4.x, u_xlat10);
        u_xlatb4 = u_xlat2.w<(-u_xlat2.w);
        u_xlat4.x = u_xlatb4 ? -3.14159274 : float(0.0);
        u_xlat21 = u_xlat21 + u_xlat4.x;
        u_xlat4.x = min(u_xlat2.w, u_xlat2.y);
        u_xlat10 = max(u_xlat2.w, u_xlat2.y);
        u_xlatb4 = u_xlat4.x<(-u_xlat4.x);
        u_xlatb10 = u_xlat10>=(-u_xlat10);
        u_xlatb4 = u_xlatb10 && u_xlatb4;
        u_xlat21 = (u_xlatb4) ? (-u_xlat21) : u_xlat21;
        u_xlat21 = fma(u_xlat21, 0.159154937, 0.5);
        u_xlat21 = fract(u_xlat21);
        u_xlat4.xy = float2(u_xlat21) * float2(6.28318024, 540.0);
        u_xlat21 = floor(FGlobals._BaseTexScale);
        u_xlat21 = u_xlat21 * u_xlat4.x;
        u_xlat2.x = sin(u_xlat21);
        u_xlat10_4.xzw = half3(_BaseTex.sample(sampler_BaseTex, u_xlat2.xz).xyz);
        u_xlat16_2 = dot(float3(u_xlat10_4.xzw), float3(0.219999999, 0.707000017, 0.0710000023));
        u_xlat18 = u_xlat18 * FGlobals._BaseColor2.w;
        u_xlat18 = u_xlat15 * u_xlat18;
        u_xlat18 = float(u_xlat16_2) * u_xlat18;
        u_xlat4.xzw = (-FGlobals._BaseColor1.xyz) + FGlobals._BaseColor2.xyz;
        u_xlat4.xzw = fma(float3(u_xlat18), u_xlat4.xzw, FGlobals._BaseColor1.xyz);
        u_xlat2.xy = u_xlat2.yw / float2(FGlobals._CapTexScale);
        u_xlat2.xy = fma(u_xlat2.xy, float2(0.5, 0.5), float2(0.5, 0.5));
        u_xlat2 = _CapTex.sample(sampler_CapTex, u_xlat2.xy);
        u_xlat20 = u_xlat2.w + -1.0;
        u_xlat20 = fma(FGlobals._CapColor.w, u_xlat20, 1.0);
        u_xlat2.xyz = fma(FGlobals._CapColor.xyz, u_xlat2.xyz, (-u_xlat4.xzw));
        u_xlat2.xyz = fma(float3(u_xlat20), u_xlat2.xyz, u_xlat4.xzw);
        u_xlat20 = sin(u_xlat4.y);
        u_xlat20 = u_xlat20 * u_xlat9.x;
        u_xlat20 = fma(u_xlat20, 0.5, 0.400000006);
        u_xlat20 = u_xlat20 * 2.5;
        u_xlat20 = clamp(u_xlat20, 0.0f, 1.0f);
        u_xlat9.x = fma(u_xlat20, -2.0, 3.0);
        u_xlat20 = u_xlat20 * u_xlat20;
        u_xlat20 = u_xlat20 * u_xlat9.x;
        u_xlat9.xyz = float3(u_xlat20) * FGlobals._BottomColor.xyz;
        u_xlat5.xyz = fma(FGlobals._BottomColor.xyz, float3(u_xlat20), (-u_xlat4.xzw));
        u_xlat4.xyz = fma(float3(u_xlat19), u_xlat5.xyz, u_xlat4.xzw);
        u_xlat5.xyz = u_xlat2.xyz + (-u_xlat4.xyz);
        u_xlat4.xyz = fma(u_xlat3.xxx, u_xlat5.xyz, u_xlat4.xyz);
        u_xlat20 = FGlobals._Time.y * 0.5;
        u_xlat20 = sin(u_xlat20);
        u_xlat20 = fma(u_xlat20, 0.200000003, 0.800000012);
        u_xlat18 = fma(u_xlat18, 0.5, 0.5);
        u_xlat0.xyz = float3(u_xlat18) * u_xlat4.xyz;
        u_xlat2.xyz = u_xlat3.xxx * u_xlat2.xyz;
        u_xlat3.xyz = float3(u_xlat19) * u_xlat9.xyz;
        u_xlat3.xyz = u_xlat3.xyz * float3(FGlobals._Underglow);
        u_xlat2.xyz = fma(u_xlat2.xyz, float3(FGlobals._Capglow), u_xlat3.xyz);
        u_xlat1.xyz = float3(u_xlat20) * u_xlat2.xyz;
    }
    u_xlat18 = FGlobals.unity_OneOverOutputBoost;
    u_xlat18 = clamp(u_xlat18, 0.0f, 1.0f);
    u_xlat0.xyz = log2(u_xlat0.xyz);
    u_xlat0.xyz = u_xlat0.xyz * float3(u_xlat18);
    u_xlat0.xyz = exp2(u_xlat0.xyz);
    u_xlat0.xyz = min(u_xlat0.xyz, float3(FGlobals.unity_MaxOutputValue));
    u_xlat0.w = 1.0;
    u_xlat0 = (FGlobals.unity_MetaFragmentControl.x) ? u_xlat0 : float4(0.0, 0.0, 0.0, 0.0);
    u_xlatb19 = float(0.0)!=FGlobals.unity_UseLinearSpace;
    u_xlat2.xyz = fma(u_xlat1.xyz, float3(0.305306017, 0.305306017, 0.305306017), float3(0.682171106, 0.682171106, 0.682171106));
    u_xlat2.xyz = fma(u_xlat1.xyz, u_xlat2.xyz, float3(0.0125228781, 0.0125228781, 0.0125228781));
    u_xlat2.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat1.xyz = (bool(u_xlatb19)) ? u_xlat1.xyz : u_xlat2.xyz;
    u_xlat1.w = 1.0;
    output.SV_Target0 = (FGlobals.unity_MetaFragmentControl.y) ? u_xlat1 : u_xlat0;
    return output;
}
                                 FGlobals         _Time                            _WorldSpaceCameraPos                     	   _CapColor                     �      _BaseColor1                   �      _BaseColor2                   �      _BottomColor                  �      _CapTexScale                  �      _BaseTexScale                     �   
   _Underglow                    �      _Capglow                  �      unity_MetaFragmentControl                    �      unity_OneOverOutputBoost                  �      unity_MaxOutputValue                  �      unity_UseLinearSpace                  �      unity_ObjectToWorld                         unity_WorldToObject                  `             _BaseTex                 _CapTex                  FGlobals           